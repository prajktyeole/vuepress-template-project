# VuePress Template Project

Vue press is a Static site generator based on Vue Js.

I have created a template project with following features in it -

[ ] Blog Setup
[ ] Pages Setup
[ ] SCSS
[ ] i18n
[ ] arabic
[ ] search
[ ] navbar
[ ] data files
[ ] GA
[ ] Syntax Highligter 
[ ] TOC
[ ] CSS/JS pipeline
[ ] CSS/JS fingerprinting
[ ] gzip assets